$(function () {
  function addClass(el, custClass) {
    el.classList.add(custClass);
  }

  function rmClass(el, custClass) {
    el.classList.remove(custClass);
  }

  function controlMenu() {
    var navBtn = document.querySelector('.nav-btn'),            // hamburger btn
        menuContainer = document.querySelector('.mobile-nav'),  // menu container
        btnIcon = navBtn.firstElementChild,                     // font awesome icon
        menuContainerActiveClass = 'mobile-nav--active',        // active class for show menu container
        hamburgerClass = 'fa-bars',
        closingClass = 'fa-times',
        enable = false; //state

    function setState(value) {
      return value;
    }

    //callback function
    function actionToMenu() {
      //if close state
      if(!enable) {
        rmClass(btnIcon, hamburgerClass);   // remove hamburger
        addClass(btnIcon, closingClass);    // add closing class

        addClass(menuContainer, menuContainerActiveClass); // show menu

        enable = setState(true);
      } else { //else open state
        rmClass(btnIcon, closingClass);     // remove closing class
        addClass(btnIcon, hamburgerClass);  // add hamburger

        rmClass(menuContainer, menuContainerActiveClass); // hide menu

        enable = setState(false);
      }
    }

    navBtn.addEventListener('click', actionToMenu);
  }
  controlMenu();

  // open lower menu level
  function openLowerMenuLevel() {
    var nextLevelBtn = document.querySelectorAll('.mobile-nav__arrow--right'),
        prevLevelBtn = document.querySelectorAll('.mobile-nav__arrow--left'),
         activeClass = 'mobile-nav__list--active';

    Array.prototype.forEach.call(nextLevelBtn, function(el) {
      el.addEventListener('click', function() {
        var lowerMenu = this.nextElementSibling;

        if(lowerMenu)
          addClass(lowerMenu, activeClass);
      });
    });

    Array.prototype.forEach.call(prevLevelBtn, function(el) {
      el.addEventListener('click', function() {
        var activeLowerMenu = this.parentElement.parentElement;

        if(activeLowerMenu)
          rmClass(activeLowerMenu, activeClass);
      });
    });
  }
  openLowerMenuLevel();
});
