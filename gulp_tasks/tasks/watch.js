var gulp = require('gulp');
var	watcher = require('gulp-watch');
var	runSequence = require('run-sequence');
var browserSync = require('./browserSync');
var config = require('../config');

var isFast = config.env.isFast;
var isLocal = config.env.isLocal;
var watchPage = config.env.watchPage;

var paths = isLocal ? config.localPaths : config.paths;

gulp.task('watch', function() {
	watcher(paths.blocks + '**/*.jade', function () {
		runSequence('concat:jade', function () {
			if(watchPage){
				gulp.start('jade:page', ['concat:jade']);
			}
		});
	});

	if(watchPage) {
		watcher(paths.pages + '**/*.jade', function(){
			gulp.start('jade:page');
		});
	}

	watcher([paths.blocks + '**/*.scss'], function () {
		gulp.start('concat:scss');
	});

	watcher(paths.blocks + '**/*.js', function () {
		gulp.start('concat:blockJs');
	});

	watcher(paths.localjs + '**/*.jade', function () {
		gulp.start('concat:js');
	});

	watcher(paths.localjs + '**/*.js', function () {
		gulp.start('concat:otherJs');
	});

	watcher(paths.localimg + '**/*.*', function () {
		gulp.start('concat:copyimg');
	});

	watcher(paths.localfonts + '**/*.scss', function () {
		gulp.start('concat:fonts');
	});

	watcher(paths.localfonts + '**/*.*', function () {
		gulp.start('concat:copyfonts');
	});

	watcher([paths.sass + '**/*.scss', paths.tmp + '**/*.scss'], function () {
		gulp.start('sass');
	});

	if(!isFast) {
		watcher([paths.pages + '**/*.jade', paths.blocks + '**/*.jade'], function () {
			runSequence('jade', function () {
				if(isLocal) browserSync.reload();
			});
		});
	}
});
